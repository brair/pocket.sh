# pocket.sh
This set of scripts contains all the necessary tools to work with the application on production server
***
### config
1. create file `pocket.sh` in `app/bin/`
2. put this code into file
```shell script
#!/bin/bash

### init
ROOT_DIR="./_pocket"
APP_DIR="../"

source "../vendor/brair/pocket.sh/main.sh"
```
3. get pocket.sh
```
composer req brair/pocket.sh:latest
```
4. Now you can usage pocket.sh commands
***

### main
```shell script
    usage: ./pocket.sh [ app | git | docker ]

    Examples:
      ./main.sh app              # use application commands
      ./main.sh git              # use git commands
      ./main.sh docker           # use docker commands
```

### app
```shell script
    usage: ./pocket.sh app [ install | reset | update ]

    Examples:
      ./main.sh app install           # install dependency and run docker containers
      ./main.sh app reset             # remove dependency and all docker containers & images
      ./main.sh app update            # pull origin from repository
```

### docker
```shell script
    usage: ./pocket.sh docker [ start | reset ]

    Examples:
      ./main.sh docker start             # run all containers for app
      ./main.sh docker reset             # remove all docker containers & images
```

### git
```shell script
    usage: ./pocket.sh git [ release ]

    Examples:
      ./main.sh git release             # create new release from dev branch and merge to master
```

***
### other
##### usage functions in scripts
```shell script
    usage: message "EXAMPLE" [ -r | -b | -m ]

    Examples:
      showInfo "EXAMPLE" -r        # console message with red backgound
      showInfo "EXAMPLE" -b        # console message with blue backgound
      showInfo "EXAMPLE" -m        # console message with magenta backgound
```
